module.exports = {
    apps: [
        {
            name: 'clusterjobs nodejs express server',
            exec_mode: 'cluster',
            instances: 2,
            script: './src/index.js',
            autorestart: true,
            watch: true,
            max_memory_restart: '1G',
        },
    ],
};
