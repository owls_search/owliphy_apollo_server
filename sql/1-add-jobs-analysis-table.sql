create table jobs_analysis_articles
(
    id              int unsigned auto_increment
        primary key,
    code            varchar(191) not null,
    language        varchar(2)   not null,
    programming     varchar(400) not null,
    location        varchar(400) not null,
    html_article    text         not null
)
    collate = utf8mb4_unicode_ci;

