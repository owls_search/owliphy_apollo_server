const { models } = require('../models');
const LIMIT = 5000;

module.exports = async function(req, res) {
    const { index = 0 } = req.query;
    const offers = await models.Offers.findAll({
        attributes: ['id', 'title', 'code'],
        offset: index * LIMIT,
        limit: LIMIT,
    });
    res.send(
        JSON.stringify(
            offers
                .map(el => el.get({ plain: true }))
                .map(({ id, title, code }) => ({
                    code: code
                        ? `${id}-${code}`
                        : `${id}-${title.replace(' ', '-').toLowerCase()}`,
                }))
        )
    );
};
