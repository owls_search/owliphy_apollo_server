const { startTestServer, getUserToken } = require('./utils/index.js');
const { models } = require('./utils/dbMock.js');
const gql = require('graphql-tag');

const offerToAdd = {
    language: 'de',
    email: 'testEMAIL-159@test.test',
    url: 'https://company.trivago.com/jobs/r0001505/',
    destinations: 'Germany, Düsseldorf',
    title: 'Developer',
    description:
        'IT support/Media/Projects – Leipzig\n' +
        '\n' +
        'We’re seeking a full-time professional to join our team in Leipzig, Germany. In this role, you’ll support an international team with any technical issues that come up, rapidly learn about state of the art technology and work on challenging projects to ensure we stay a trend setter and not a trend follower.\n' +
        '\n' +
        '    What you’ll do:\n' +
        '\n' +
        '    Install and maintain workstations.\n' +
        '    Support and maintain media equipment.\n' +
        '    Support our international users by troubleshooting of IT malfunctions and being the first point of contact for all IT-related issues or queries.\n' +
        '    Inventory, stock control and storage maintenance.\n' +
        '    Align and collaborate with our networking team and media team in head office.\n' +
        '    Work on challenging projects to lift our infrastructure to the next level.\n' +
        '\n' +
        '    What you’ll definitely need:\n' +
        '\n' +
        '    Good knowledge of Windows 10/MS Office and basic knowledge of networking.\n' +
        '    At least some familiarity with MacOS and/or Linux.\n' +
        '    Speaking our company language English fluently.\n' +
        '    Good digital communication skills.\n' +
        '    Possessing a very systematic method of working and are skilled in managing your time.\n' +
        '    The ability to complete assignments even under extremely challenging circumstances and high-pressure situations.\n' +
        '    You exhibit the ability to adapt very quickly to changes.\n' +
        '    Working well with others in progressing towards common goals.\n' +
        '    Being self-motivated and capable of working independently.\n' +
        '\n' +
        '    Life at trivago is:\n' +
        '\n' +
        '    A unique culture with a strong sense of community and an agile, international work environment.\n' +
        '\n' +
        '    The opportunity for self-driven individuals to have a direct impact on the business.\n' +
        '\n' +
        '    The freedom to embrace small-scale failures as a path to large-scale success.\n' +
        '\n' +
        '    The belief that factual proof is the driving force behind all decisions and determines the way forward.\n' +
        '\n' +
        '    The chance to develop personally and professionally due to a strong feedback culture and access to training and workshops.\n' +
        '\n' +
        '    Flexibility for all talents to contribute value and maintain a healthy work-life balance.\n' +
        '\n' +
        '    A state-of-the-art campus with world-class ergonomics that supports your health and happiness, 30+ sports and a multi-cuisine cafeteria to satisfy your inner foodie.\n' +
        '\n' +
        '    To find out more about life at trivago, follow us on social media @lifeattrivago.',
    miniDescription:
        'We’re seeking a full-time professional to join our team in Leipzig, Germany. In this role, you’ll support an international team with any technical issues that come up, rapidly learn about state of the art technology and work on challenging projects to ensure we stay a trend setter and not a trend follower.',
    startsFrom: '2019-01-14',
    salaryFrom: 30000,
    salaryTo: 50000,
    companyName: 'trivago N.V.',
    offerType: 'full',
};

const ADD_OFFER = gql`
    mutation addOffer($token: String!, $offer: OfferInput!) {
        addOffer(token: $token, offer: $offer) {
            id
        }
    }
`;

const UPDATE_OFFER = gql`
    mutation updateOffer($token: String!, $id: ID!, $offer: OfferInput!) {
        updateOffer(token: $token, id: $id, offer: $offer) {
            message
        }
    }
`;

const DELETE_OFFER = gql`
    mutation deleteOffer($id: ID!, $token: String!) {
        deleteOffer(id: $id, token: $token) {
            message
        }
    }
`;

const GET_OFFER = gql`
    query offer($id: ID!) {
        offer(id: $id) {
            title
            url
            language
            skills
            programmingLanguages
            benefits
            bid
            partnerParam
            partnerValue
            importPartnerName
            similarOffers {
                title
            }
        }
    }
`;

const GET_USER_OFFERS = gql`
    query getUserOffers($page: Int!, $token: String!) {
        getUserOffers(page: $page, token: $token) {
            offers {
                title
                url
                language
                skills
                programmingLanguages
                benefits
                bid
                partnerParam
                partnerValue
                importPartnerName
            }
            totalNumber
        }
    }
`;

const GET_HOMEPAGE_CONFIG = gql`
    query getHomepageConfiguration($locale: String!) {
        getHomepageConfiguration(locale: $locale) {
            recentOffers {
                title
                url
                language
                skills
                programmingLanguages
                benefits
                bid
                partnerParam
                partnerValue
                importPartnerName
            }
        }
    }
`;

describe('Server - offers', () => {
    const gql = {};

    beforeAll(async () => {
        const { query, mutate } = await startTestServer();
        gql.mutate = mutate;
        gql.query = query;
    });

    it('should be able to add offer', async () => {
        expect.assertions(1);
        const token = await getUserToken(gql.query);
        return expect(
            gql.mutate({
                query: ADD_OFFER,
                variables: {
                    token,
                    offer: offerToAdd,
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should be able to update offer', async () => {
        expect.assertions(1);
        const token = await getUserToken(gql.query);
        models.Offers.$queueResult(
            models.Offers.build({
                id: 123,
            })
        );
        return expect(
            gql.mutate({
                query: UPDATE_OFFER,
                variables: {
                    token,
                    id: 123,
                    offer: offerToAdd,
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should get error when offer not found', async () => {
        expect.assertions(1);
        const token = await getUserToken(gql.query);
        models.Offers.$queueResult({});
        return expect(
            gql.mutate({
                query: UPDATE_OFFER,
                variables: {
                    token,
                    id: 79949889494946465546565,
                    offer: offerToAdd,
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should be able to delete offer', async () => {
        expect.assertions(1);
        const token = await getUserToken(gql.query);
        models.Offers.$queueResult(1);
        return expect(
            gql.mutate({
                query: DELETE_OFFER,
                variables: {
                    id: 123,
                    token,
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should not delete offer if not found', async () => {
        expect.assertions(1);
        const token = await getUserToken(gql.query);
        models.Offers.$queueResult(0);
        return expect(
            gql.mutate({
                query: DELETE_OFFER,
                variables: {
                    id: 123,
                    token,
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should be able to get offer data', async () => {
        expect.assertions(1);
        models.Offers.$queueResult(models.Offers.build(offerToAdd));
        return expect(
            gql.mutate({
                query: GET_OFFER,
                variables: {
                    id: 123,
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should be able to get all user offers data', async () => {
        expect.assertions(1);
        const token = await getUserToken(gql.query);
        models.Offers.$queueResult([models.Offers.build(offerToAdd)]);
        return expect(
            gql.mutate({
                query: GET_USER_OFFERS,
                variables: {
                    page: 0,
                    token,
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should be able to get recently added offers', async () => {
        expect.assertions(1);
        models.Offers.$queueResult([models.Offers.build(offerToAdd)]);
        models.JobsAnalysisArticles.$queueResult([]);
        return expect(
            gql.mutate({
                query: GET_HOMEPAGE_CONFIG,
                variables: {
                    locale: 'de',
                },
            })
        ).resolves.toMatchSnapshot();
    });
});
