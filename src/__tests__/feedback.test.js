const { startTestServer } = require('./utils/index.js');
const gql = require('graphql-tag');

const ADD_FEEDBACK = gql`
    mutation addFeedback($email: String!, $message: String!, $name: String!) {
        addFeedback(email: $email, message: $message, name: $name) {
            message
        }
    }
`;

describe('Server - add feedback', () => {
    const gql = {};

    beforeAll(async () => {
        const { query, mutate } = await startTestServer();
        gql.mutate = mutate;
        gql.query = query;
    });

    it('should add feedback data', async () => {
        expect.assertions(1);
        return expect(
            gql.mutate({
                query: ADD_FEEDBACK,
                variables: {
                    email: 'test@tat.a',
                    message: 'some message',
                    name: 'guy',
                },
            })
        ).resolves.toMatchSnapshot();
    });
});
