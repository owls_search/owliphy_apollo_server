const { startTestServer, addUser, getUserToken } = require('./utils/index.js');
const gql = require('graphql-tag');
const { models } = require('./utils/dbMock.js');

const REGISTER_USER = gql`
    mutation registerUser(
        $email: String!
        $emailConfirmation: String!
        $password: String!
        $type: String!
    ) {
        registerUser(
            email: $email
            emailConfirmation: $emailConfirmation
            password: $password
            type: $type
        ) {
            user {
                name
                lastName
                email
                type
            }
        }
    }
`;

const LOGIN_USER = gql`
    query loginUser($email: String!, $password: String!) {
        loginUser(email: $email, password: $password) {
            user {
                name
                lastName
                email
                type
            }
        }
    }
`;

const GET_USER_DATA = gql`
    query user($token: String!) {
        user(token: $token) {
            name
            lastName
            email
            type
        }
    }
`;

const UPDATE_USER_PASSWORD = gql`
    mutation updatePassword(
        $token: String!
        $password: String!
        $newPassword: String!
    ) {
        updatePassword(
            token: $token
            password: $password
            newPassword: $newPassword
        ) {
            message
        }
    }
`;

const DELETE_USER = gql`
    mutation deleteUser($token: String!) {
        deleteUser(token: $token) {
            message
        }
    }
`;

const UPDATE_ACCOUNT_INFO = gql`
    mutation updateAccountInfo(
        $token: String!
        $name: String
        $lastName: String
        $email: String
        $partnerParam: String
        $partnerValue: String
    ) {
        updateAccountInfo(
            token: $token
            name: $name
            lastName: $lastName
            email: $email
            partnerParam: $partnerParam
            partnerValue: $partnerValue
        ) {
            name
            lastName
            type
            email
        }
    }
`;

const UPDATE_PROFILE_INFO = gql`
    mutation updateProfileInfo(
        $token: String!
        $skills: String
        $programmingLanguages: String
        $titles: String
        $locations: String
        $offerType: String
        $file: String
    ) {
        updateProfileInfo(
            token: $token
            skills: $skills
            programmingLanguages: $programmingLanguages
            titles: $titles
            locations: $locations
            offerType: $offerType
            fileName: $file
        ) {
            name
            lastName
            type
            email
        }
    }
`;

describe('Server - user related api', () => {
    const gql = {};

    beforeAll(async () => {
        const { query, mutate } = await startTestServer();
        gql.mutate = mutate;
        gql.query = query;
    });

    it('should register user', async () => {
        expect.assertions(1);
        await models.Users.$queueResult({});
        return expect(
            gql.mutate({
                query: REGISTER_USER,
                variables: {
                    email: 'test@test.test',
                    emailConfirmation: 'test@test.test',
                    password: 'asd123',
                    type: 'applicant',
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should login user', async () => {
        expect.assertions(1);

        await addUser({ email: 'test@test.test' }, 'asd123');
        return expect(
            gql.query({
                query: LOGIN_USER,
                variables: {
                    email: 'test@test.test',
                    password: 'asd123',
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should get user data', async () => {
        expect.assertions(1);
        const token = await getUserToken(gql.query);
        await addUser({ email: 'test@test.test', name: 'test' }, 'asd123');
        return expect(
            gql.mutate({
                query: GET_USER_DATA,
                variables: {
                    token,
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should update user password', async () => {
        expect.assertions(1);
        const token = await getUserToken(gql.query);
        await addUser({ email: 'test@test.test', name: 'test' }, 'asd123');
        return expect(
            gql.mutate({
                query: UPDATE_USER_PASSWORD,
                variables: {
                    token,
                    password: 'asd123',
                    newPassword: 'blah balh',
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should not update user password if pass is wrong', async () => {
        expect.assertions(1);
        const token = await getUserToken(gql.query);
        await addUser({ email: 'test@test.test', name: 'test' }, 'asd123');
        return expect(
            gql.mutate({
                query: UPDATE_USER_PASSWORD,
                variables: {
                    token,
                    password: 'afassds',
                    newPassword: 'blah balh',
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should delete user', async () => {
        expect.assertions(1);

        const token = await getUserToken(gql.query);
        await addUser({ email: 'test@test.test', name: 'test' }, 'asd123');
        return expect(
            gql.mutate({
                query: DELETE_USER,
                variables: {
                    token,
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should update user account info', async () => {
        expect.assertions(1);

        const token = await getUserToken(gql.query);
        await addUser(
            {
                name: 'test',
                lastName: 'test',
                email: 'test123123@asd.asd',
                partnerParam: 'ppp',
                partnerValue: '123',
            },
            'asd123'
        );
        return expect(
            gql.mutate({
                query: UPDATE_ACCOUNT_INFO,
                variables: {
                    token,
                    name: 'test',
                    lastName: 'test',
                    email: 'test123123@asd.asd',
                    partnerParam: 'ppp',
                    partnerValue: '123',
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should update user profile info', async () => {
        expect.assertions(1);

        const token = await getUserToken(gql.query);
        await models.Users.$queueResult({});
        await addUser(
            {
                email: 'test@test.test',
                name: 'test old',
                skills: 'test',
                programmingLanguages: 'test',
                titles: 'test',
                locations: 'test',
                offerType: 'test',
                file: 'test',
            },
            'asd123'
        );
        return expect(
            gql.mutate({
                query: UPDATE_PROFILE_INFO,
                variables: {
                    token,
                    skills: 'test',
                    programmingLanguages: 'test',
                    titles: 'test',
                    locations: 'test',
                    offerType: 'test',
                    file: 'test',
                },
            })
        ).resolves.toMatchSnapshot();
    });
});
