const { startTestServer } = require('./utils/index.js');
const gql = require('graphql-tag');

const GET_POST = gql`
    query getPost($code: String!) {
        getPost(code: $code) {
            title
            preview
            language
            code
            postTags
            text
            seoTitle
            seoKeywords
            seoDescription
            images {
                small
                normal
                big
            }
            translatesCodes
            nextPostCode
        }
    }
`;

const GET_LAST_POSTS = gql`
    query getRecentPosts($locale: String!) {
        getRecentPosts(locale: $locale) {
            title
            preview
            language
        }
    }
`;

describe('Server - blog related api', () => {
    const gql = {};

    beforeAll(async () => {
        const { query, mutate } = await startTestServer();
        gql.mutate = mutate;
        gql.query = query;
    });

    it('should get correct fields', async () => {
        expect.assertions(1);
        return expect(
            gql.mutate({
                query: GET_POST,
                variables: {
                    code: 'default',
                    locale: 'de',
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should get last posts', async () => {
        expect.assertions(1);
        return expect(
            gql.mutate({
                query: GET_LAST_POSTS,
                variables: {
                    locale: 'en',
                },
            })
        ).resolves.toMatchSnapshot();
    });
});
