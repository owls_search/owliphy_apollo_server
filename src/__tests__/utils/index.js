const { createTestClient } = require('apollo-server-testing');
const gql = require('graphql-tag');
const { get } = require('lodash');
const getServer = require('../../server.js');
const { models } = require('./dbMock.js');
const { hashPassword } = require('../../util');

const mockApiCall = () => async () => new Promise(resolve => resolve());

const dataSources = {
    mailAPI: {
        sendAfterNewSavedSearch: mockApiCall(),
        sendAfterFeedback: mockApiCall(),
        sendToSaveSearchNewcomer: mockApiCall(),
    },

    fileAPI: {
        getFile: mockApiCall(),
        deleteFile: mockApiCall(),
    },

    searchAPI: {
        search: ({ query }) => [
            {
                externalId: 'default',
                url: 'default',
                destinations: 'default',
                description: 'default',
                htmlDescription: 'default',
                miniDescription: 'default',
                language: 'default',
                skills: 'default',
                programmingLanguages: 'default',
                title: query,
                startsFrom: null,
                salaryFrom: 42069,
                salaryTo: 69420,
                email: 'default',
                logo: 'default',
                companyName: 'default',
                createdAt: null,
                offerType: 'default',
                benefits: 'default',
                bid: 0,
            },
        ],
        similarOffers: ({
            destinations,
            programmingLanguages,
            skills,
            benefits,
        }) => [
            {
                externalId: 'default',
                url: 'default',
                destinations: 'default',
                description: 'default',
                htmlDescription: 'default',
                miniDescription: 'default',
                language: 'default',
                skills: 'default',
                programmingLanguages: 'default',
                title: `${skills} ${destinations} ${programmingLanguages} ${benefits}`,
                startsFrom: null,
                salaryFrom: 42069,
                salaryTo: 69420,
                email: 'default',
                logo: 'default',
                companyName: 'default',
                createdAt: null,
                offerType: 'default',
                benefits: 'default',
                bid: 0,
            },
        ],
    },
};

const startTestServer = async () => {
    return await createTestClient(getServer(models, dataSources));
};

const addUser = async (fields, password) => {
    await models.Users.$queueResult(
        models.Users.build({
            name: 'test',
            lastName: 'test',
            type: 'Very test',
            email: 'test@test.test',
            password: await hashPassword(password),
            ...fields,
        })
    );
};

const getUserToken = async query => {
    await addUser({ email: 'test@test.test' }, 'asd123');
    const response = await query({
        query: gql`
            query loginUser($email: String!, $password: String!) {
                loginUser(email: $email, password: $password) {
                    token
                }
            }
        `,
        variables: {
            email: 'test@test.test',
            password: 'asd123',
        },
    });

    return get(response, ['data', 'loginUser', 'token'], null);
};

module.exports = {
    startTestServer,
    addUser,
    getUserToken,
};
