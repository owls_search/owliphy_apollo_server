const extendModelAPI = require('../../../models/users/extend.js');

module.exports = function(sequelize) {
    const User = sequelize.define('users', {
        name: null,
        lastName: null,
        email: null,
        password: null,
        skills: null,
        titles: null,
        offerType: null,
        file: null,
        type: null,
        locations: null,
        programmingLanguages: null,
        partnerParam: null,
        partnerValue: null,
        partnerBid: 0,
        partnerCompanyName: null,
        partnerCompanyLogo: null,
        importPartnerName: null,
    });

    return extendModelAPI(User);
};
