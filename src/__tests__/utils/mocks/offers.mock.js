const extendModelAPI = require('../../../models/offers/extend.js');

module.exports = function(sequelize) {
    const Offers = sequelize.define('jobs', {
        externalId: 'default',
        url: 'default',
        destinations: 'default',
        description: 'default',
        htmlDescription: 'default',
        miniDescription: 'default',
        language: 'default',
        skills: 'default',
        programmingLanguages: 'default',
        title: 'default',
        startsFrom: null,
        salaryFrom: 42069,
        salaryTo: 69420,
        email: 'default',
        logo: 'default',
        companyName: 'default',
        createdAt: null,
        offerType: 'default',
        benefits: 'default',
        bid: 0,
    });

    Offers.count = () => 5;

    return extendModelAPI(Offers);
};
