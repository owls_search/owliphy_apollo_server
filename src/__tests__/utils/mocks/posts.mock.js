const extendModelAPI = require('../../../models/posts/extend.js');

module.exports = function(sequelize) {
    const Posts = sequelize.define('posts', {
        imageSmall: 'default/small/img',
        imageNormal: 'default/normal/img',
        imageBig: 'default/big/img',
        active: true,
    });

    return extendModelAPI(Posts);
};
