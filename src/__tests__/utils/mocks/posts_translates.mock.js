module.exports = function(sequelize, DataTypes) {
    let PostsTranslations = sequelize.define('posts_translates', {
        title: 'default post',
        text: 'default post',
        preview: 'default preview',
        seoTitle: 'default preview',
        seoKeywords: 'default preview',
        seoDescription: 'default preview',
        code: 'defaultPost',
        postTags: 'default blah, blah, blah',
        language: 'default en',
    });

    // noinspection JSUnresolvedVariable
    PostsTranslations.associations = {
        post: 'post',
    };

    return PostsTranslations;
};
