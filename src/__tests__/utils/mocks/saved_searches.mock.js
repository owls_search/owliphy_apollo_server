module.exports = function(sequelize) {
    return sequelize.define('saved_searches', {
        query: 'default query',
        sendDateOfMonth: '23',
        sendWeeklyMonday: false,
        sendWeeklyTuesday: false,
        sendWeeklyWednesday: false,
        sendWeeklyThursday: false,
        sendWeeklyFriday: false,
        sendWeeklySaturday: false,
        sendWeeklySunday: false,
    });
};
