const SequelizeMock = require('sequelize-mock');
const sequelize = new SequelizeMock();

const models = {
    Users: sequelize.import('./mocks/users.mock.js'),
    Offers: sequelize.import('./mocks/offers.mock.js'),
    SavedSearches: sequelize.import('./mocks/saved_searches.mock.js'),
    Posts: sequelize.import('./mocks/posts.mock.js'),
    PostsTranslations: sequelize.import('./mocks/posts_translates.mock.js'),
    Feedback: sequelize.import('./mocks/feedback.mock.js'),
    LogsDetails: sequelize.import('./mocks/logs_details.mock.js'),
    LogsEvents: sequelize.import('./mocks/logs_events.mock.js'),
    Logs: sequelize.import('./mocks/logs.mock.js'),
    LogsToDetails: sequelize.import('./mocks/logs_to_details.mock.js'),
    JobsAnalysisArticles: sequelize.import(
        './mocks/jobs_analysis_articles.mock.js'
    ),
};

Object.keys(models).forEach(key => {
    if ('associate' in models[key]) {
        models[key].associate(models);
    }
});

module.exports = { sequelize, models };
