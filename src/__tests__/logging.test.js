const { startTestServer } = require('./utils/index.js');
const gql = require('graphql-tag');

const LOG = gql`
    mutation log($event: Int!, $details: JSON!, $timestamp: Timestamp!) {
        log(event: $event, details: $details, timestamp: $timestamp)
    }
`;

describe('Server - logging', () => {
    const gql = {};

    beforeAll(async () => {
        const { query, mutate } = await startTestServer();
        gql.mutate = mutate;
        gql.query = query;
    });

    it('write logging data', async () => {
        expect.assertions(1);
        return expect(
            gql.mutate({
                query: LOG,
                variables: {
                    event: 1,
                    details: JSON.stringify({
                        4: 123,
                    }),
                    timestamp: 1234650004,
                },
            })
        ).resolves.toMatchSnapshot();
    });
});
