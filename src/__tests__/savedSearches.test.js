const { startTestServer, getUserToken } = require('./utils/index.js');
const { models } = require('./utils/dbMock.js');
const gql = require('graphql-tag');

const SAVE_SEARCH = gql`
    query saveSearchAdd(
        $token: String!
        $query: String!
        $isWeekly: Boolean!
        $dayOfMonth: String
        $dayOfWeek: String
    ) {
        saveSearchAdd(
            token: $token
            query: $query
            isWeekly: $isWeekly
            dayOfMonth: $dayOfMonth
            dayOfWeek: $dayOfWeek
        ) {
            result {
                message
            }
            savedSearch {
                query
                schedule {
                    weekDays
                }
            }
        }
    }
`;

const SAVE_SEARCH_NEWCOMER = gql`
    query saveSearchAddNewcomer(
        $email: String!
        $query: String!
        $isWeekly: Boolean!
        $dayOfMonth: String
        $dayOfWeek: String
    ) {
        saveSearchAddNewcomer(
            email: $email
            query: $query
            isWeekly: $isWeekly
            dayOfMonth: $dayOfMonth
            dayOfWeek: $dayOfWeek
        ) {
            user {
                name
                lastName
                email
                savedSearches {
                    query
                }
            }
        }
    }
`;

describe('Server - save searches', () => {
    const gql = {};

    beforeAll(async () => {
        const { query, mutate } = await startTestServer();
        gql.mutate = mutate;
        gql.query = query;
    });

    it('should save searches of not authenticated users', async () => {
        expect.assertions(1);
        await models.Users.$queueResult();
        return expect(
            gql.mutate({
                query: SAVE_SEARCH_NEWCOMER,
                variables: {
                    email: 'test@etete.as',
                    query: 'qa engineer',
                    isWeekly: true,
                    dayOfMonth: '',
                    dayOfWeek: 'Thursday',
                },
            })
        ).resolves.toMatchSnapshot();
    });

    it('should save searches of authenticated users', async () => {
        expect.assertions(1);
        const token = await getUserToken(gql.query);
        return expect(
            gql.mutate({
                query: SAVE_SEARCH,
                variables: {
                    token,
                    query: 'qa engineer',
                    isWeekly: true,
                    dayOfMonth: '',
                    dayOfWeek: 'Thursday',
                },
            })
        ).resolves.toMatchSnapshot();
    });
});
