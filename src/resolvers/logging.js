module.exports = {
    Mutation: {
        log: async (parent, { event, details, timestamp }, { models }) => {
            const log = await models.Logs.create({
                timestamp: timestamp.getTime(),
                eventId: event,
            });

            await Object.keys(details).forEach(async function(detail) {
                await models.LogsToDetails.create({
                    logId: log.id,
                    detailId: detail,
                    value: details[detail],
                });
            });

            return true;
        },
    },
};
