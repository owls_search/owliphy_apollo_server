const {
    RESPONSE_MESSAGES,
    retrieveToken,
    randomPassword,
    createToken,
} = require('../util');
const { omitBy, identity, negate, lowerCase } = require('lodash');
const { UserInputError, ApolloError } = require('apollo-server');

const WEEKLY_FIELDS = {
    monday: 'sendWeeklyMonday',
    tuesday: 'sendWeeklyTuesday',
    wednesday: 'sendWeeklyWednesday',
    thursday: 'sendWeeklyThursday',
    friday: 'sendWeeklyFriday',
    saturday: 'sendWeeklySaturday',
    sunday: 'sendWeeklySunday',
};

const getSendPeriodData = (isWeekly, dayOfMonth, dayOfWeek) => {
    if (!isWeekly) {
        const leadingZeroDates = dayOfMonth
            .split(',')
            .map(date => {
                const strDate = date + '';
                return strDate.length >= 2 ? strDate : `0${strDate}`;
            })
            .join(',');

        return {
            sendDateOfMonth: leadingZeroDates,
            sendWeeklyMonday: false,
            sendWeeklyTuesday: false,
            sendWeeklyWednesday: false,
            sendWeeklyThursday: false,
            sendWeeklyFriday: false,
            sendWeeklySaturday: false,
            sendWeeklySunday: false,
        };
    }

    const updateFields = {};
    const userNewDays = dayOfWeek.split(',').map(lowerCase);

    Object.keys(WEEKLY_FIELDS).forEach(weekDay => {
        updateFields[WEEKLY_FIELDS[weekDay]] = userNewDays.includes(weekDay.toLowerCase());
    });

    return {
        sendDateOfMonth: null,
        ...updateFields
    };
};

module.exports = {
    Query: {
        saveSearchAdd: async (
            parent,
            { token, query, isWeekly, dayOfMonth, dayOfWeek },
            { models, dataSources }
        ) => {
            const { id } = await retrieveToken(token);

            const sendPeriodFields = getSendPeriodData(
                isWeekly,
                dayOfMonth,
                dayOfWeek
            );

            if (Object.keys(sendPeriodFields).length === 0) {
                throw new ApolloError(RESPONSE_MESSAGES.SAVE_SEARCH_FAILED);
            }

            const savedSearch = await models.SavedSearches.create({
                ...sendPeriodFields,
                userId: id,
                query,
            });

            const { email } = await models.Users.findById(id);
            await dataSources.mailAPI.sendAfterNewSavedSearch(
                email,
                query
            );

            return {
                result: { message: RESPONSE_MESSAGES.SAVE_SEARCH_SUCCESS },
                savedSearch
            };
        },
        saveSearchRemove: async (
            parent,
            { token, id },
            { models }
        ) => {
            const { id: userId } = await retrieveToken(token);

            const removedRows = await models.SavedSearches.destroy({
                where: {
                    userId,
                    id
                }
            });

            if (!removedRows) {
                throw new UserInputError(RESPONSE_MESSAGES.SAVE_SEARCH_FAILED);
            }

            return {
                message: RESPONSE_MESSAGES.SAVE_SEARCH_REMOVE_SUCCESS,
            };
        },
        saveSearchUpdate: async (
            parent,
            { token, id, query, isWeekly, dayOfMonth, dayOfWeek },
            { models }
        ) => {
            if (id <= 0) {
                throw new UserInputError(RESPONSE_MESSAGES.SAVE_SEARCH_FAILED);
            }

            const { id: userId } = await retrieveToken(token);

            const sendPeriodFields = getSendPeriodData(
                isWeekly,
                dayOfMonth,
                dayOfWeek
            );

            if (Object.keys(sendPeriodFields).length === 0) {
                throw new ApolloError(RESPONSE_MESSAGES.SAVE_SEARCH_FAILED);
            }

            const updateFields = sendPeriodFields;
            if (query) {
                updateFields.query = query;
            }

            await models.SavedSearches.update(
                updateFields,
                {
                    where: {
                        id: parseInt(id, 10),
                        userId
                    },
                });

            const savedSearch = await models.SavedSearches.findOne({
                where: { id },
            });

            return {
                result: { message: RESPONSE_MESSAGES.SAVE_SEARCH_UPDATE_SUCCESS },
                savedSearch
            };
        },
        saveSearchAddNewcomer: async (
            parent,
            { email, query, isWeekly, dayOfMonth, dayOfWeek },
            { models, dataSources }
        ) => {
            const password = randomPassword(8);
            const user = await models.Users.createUser(email, password, {
                type: 'applicant',
            });

            const sendPeriodFields = getSendPeriodData(
                isWeekly,
                dayOfMonth,
                dayOfWeek
            );

            if (Object.keys(sendPeriodFields).length === 0) {
                throw new ApolloError(RESPONSE_MESSAGES.SAVE_SEARCH_FAILED);
            }

            await models.SavedSearches.create({
                ...sendPeriodFields,
                userId: user.id,
                query,
            });

            await dataSources.mailAPI.sendToSaveSearchNewcomer(
                email,
                query,
                password
            );

            const updatedUser = await models.Users.findByEmail(email);
            return {
                token: await createToken(updatedUser),
                user: updatedUser,
            };
        },
    },

    SavedSearch: {
        schedule: async search => {
            const {
                sendDateOfMonth,
                sendWeeklyMonday,
                sendWeeklyTuesday,
                sendWeeklyWednesday,
                sendWeeklyThursday,
                sendWeeklyFriday,
                sendWeeklySaturday,
                sendWeeklySunday,
            } = search;

            const userWeekDays = Object.keys(
                omitBy(
                    {
                        monday: sendWeeklyMonday,
                        tuesday: sendWeeklyTuesday,
                        wednesday: sendWeeklyWednesday,
                        thursday: sendWeeklyThursday,
                        friday: sendWeeklyFriday,
                        saturday: sendWeeklySaturday,
                        sunday: sendWeeklySunday,
                    },
                    negate(identity)
                )
            );

            return {
                isWeekly: userWeekDays.length > 0,
                weekDays: userWeekDays,
                monthDays:
                    (sendDateOfMonth && sendDateOfMonth.split(',')) || [],
            };
        },
    },
};
