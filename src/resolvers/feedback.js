const { RESPONSE_MESSAGES } = require('../util');

module.exports = {
    Mutation: {
        addFeedback: async (
            parent,
            { email, message, name },
            { models, dataSources }
        ) => {
            await models.Feedback.create({
                email,
                message,
                name,
            });

            await dataSources.mailAPI.sendAfterFeedback(email, message);
            return { message: RESPONSE_MESSAGES.FEEDBACK_SUCCESS };
        },
    },
};
