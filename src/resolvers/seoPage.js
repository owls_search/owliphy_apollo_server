const { Op } = require('sequelize');
const { h1ToH2 } = require('../util');

module.exports = {
    Query: {
        getSeoPage: async (parent, { code }, { models }) => {
            const seoPage = await models.JobsAnalysisArticles.findOne({
                where: { code },
            });

            return (
                seoPage &&
                seoPage.dataValues && {
                    ...seoPage.dataValues,
                    content: h1ToH2(seoPage.dataValues.content || ''),
                }
            );
        },
        findPopularJobs: (parent, { city, lang }, { models }) => {
            const queryLang = ['en', 'de'].includes(lang) ? lang : 'de';
            const location = ['München', 'Munich'].includes(city)
                ? {
                      [Op.or]: ['München', 'Munich'],
                  }
                : city;

            return models.JobsAnalysisArticles.findAll({
                where: { location, language: queryLang },
            });
        },
    },
};
