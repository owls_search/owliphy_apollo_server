const { RESPONSE_MESSAGES, retrieveToken, h1ToH2 } = require('../../util');
const { isEmpty, head } = require('lodash');
const { Op } = require('sequelize');

const filterOutH1 = offer => ({
    ...offer,
    htmlDescription: h1ToH2(offer.htmlDescription || ''),
});

module.exports = {
    Query: {
        getUserOffers: async (parent, { page = 0, token }, { models }) => {
            const { id } = await retrieveToken(token);
            const offers = await models.Offers.findAll({
                where: {
                    createdUserId: id,
                },
                offset: page * 15,
                limit: 15,
            });

            const totalNumber = await models.Offers.count({
                where: {
                    createdUserId: id,
                },
            });

            return { offers, totalNumber };
        },
        getHomepageConfiguration: async (
            parent,
            { locale },
            { models, dataSources },
            info
        ) => {
            info.cacheControl.setCacheHint({
                maxAge: 15 * 60,
            });

            let partnerOffers = [];
            let ownOffers = [];
            try {
                partnerOffers = await dataSources.searchAPI.recentOffers();
            } catch (e) {
                partnerOffers = [];
            }

            if (partnerOffers.length <= 8) {
                ownOffers = await models.Offers.findAll({
                    where: {
                        isIndexed: 1,
                    },
                    order: [['dynamic_bid', 'DESC'], ['updatedAt', 'DESC']],
                    limit: 8,
                });
            }

            const queryLang = ['en', 'de'].includes(locale) ? locale : 'de';
            const seoPages = await models.JobsAnalysisArticles.findAll({
                where: {
                    language: queryLang,
                },
                order: [['code', 'DESC']],
            });

            return {
                recentOffers: [...partnerOffers, ...ownOffers].slice(0, 8),
                seoPages,
            };
        },
        offer: async (parent, { id }, { models }) => {
            const offerData = await models.Offers.findById(id);
            return (
                offerData &&
                offerData.dataValues &&
                filterOutH1(offerData.dataValues)
            );
        },
    },

    Mutation: {
        addOffer: async (parent, { token, offer }, { models }) => {
            const { id } = await retrieveToken(token);
            const newOffer = await models.Offers.create({
                createdUserId: id,
                ...offer,
                startsFrom: offer.startsFrom.length ? offer.startsFrom : null,
            });
            return { id: newOffer.id };
        },
        updateOffer: async (parent, { token, id, offer }, { models }) => {
            const { id: userId } = await retrieveToken(token);
            const updateOffer = await models.Offers.update(offer, {
                where: {
                    createdUserId: userId,
                    id,
                },
                returning: true,
                plain: true,
            });

            if (isEmpty(head(updateOffer))) {
                return { message: RESPONSE_MESSAGES.JOB_NOT_FOUND };
            }

            return { message: RESPONSE_MESSAGES.JOB_SUCCESS_UPDATE };
        },
        deleteOffer: async (parent, { id, token }, { models }) => {
            const { id: userId } = await retrieveToken(token);
            const removedRows = await models.Offers.destroy({
                where: {
                    createdUserId: userId,
                    id,
                },
            });

            if (!removedRows) {
                return { message: RESPONSE_MESSAGES.JOB_NOT_FOUND };
            }

            return { message: RESPONSE_MESSAGES.JOB_SUCCESS_REMOVE };
        },
    },

    Offer: {
        similarOffers: async (offer, args, { dataSources }) => {
            const {
                programmingLanguages = '',
                skills = '',
                destinations = '',
                benefits = '',
                title = '',
                id: searchedOfferId,
            } = offer;

            try {
                const similarOffers = await dataSources.searchAPI.similarOffers(
                    {
                        destinations,
                        programmingLanguages,
                        skills,
                        benefits,
                        title,
                    }
                );

                return similarOffers
                    .filter(
                        ({ id }) =>
                            parseInt(id, 10) !== parseInt(searchedOfferId, 10)
                    )
                    .map(filterOutH1);
            } catch (e) {
                return [];
            }
        },
        popularJobs: (offer, args, { models }) => {
            const { destinations = '' } = offer;

            const location = ['München', 'Munich'].includes(destinations)
                ? {
                      [Op.or]: ['München', 'Munich'],
                  }
                : destinations;

            return models.JobsAnalysisArticles.findAll({
                where: { location },
            });
        },
    },
};
