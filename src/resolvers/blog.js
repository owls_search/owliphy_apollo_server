const { Op } = require('sequelize');
const { get } = require('lodash');

module.exports = {
    Query: {
        getRecentPosts: (parent, { locale }, { models }, info) => {
            info.cacheControl.setCacheHint({
                maxAge: 6 * 60 * 60,
            });

            return models.PostsTranslations.findAll({
                where: {
                    language: locale,
                },
                include: [
                    {
                        model: models.Posts,
                    },
                ],
                order: [
                    [
                        models.PostsTranslations.associations.post,
                        'updatedAt',
                        'DESC',
                    ],
                ],
                limit: 15,
            });
        },
        getPost: (parent, { code }, { models }) => {
            return models.PostsTranslations.findOne({
                where: { code },
            });
        },
    },

    PostObject: {
        images: async (postTranslation, args, { models }) => {
            const {
                imageSmall,
                imageNormal,
                imageBig,
            } = await models.Posts.findById(postTranslation.postId);

            return {
                small: imageSmall,
                normal: imageNormal,
                big: imageBig,
            };
        },
        translatesCodes: async (postTranslation, args, { models }) => {
            const posts = await models.PostsTranslations.findAll({
                attributes: ['language', 'code'],
                where: {
                    postId: postTranslation.postId,
                },
            });

            const codeToLang = {};
            posts.forEach(({ language, code }) => {
                codeToLang[language] = code;
            });

            return codeToLang;
        },
        nextPostCode: async (postTranslation, args, { models }) => {
            const post = await models.PostsTranslations.findOne({
                attributes: ['code'],
                where: {
                    postId: {
                        [Op.lt]: postTranslation.postId,
                    },
                    language: postTranslation.language,
                },
                order: [['id', 'DESC']],
            });

            return get(post, 'code', null);
        },
    },
};
