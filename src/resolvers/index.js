const feedbackResolvers = require('./feedback');
const userResolvers = require('./user');
const offersResolvers = require('./offers');
const savedSearchResolvers = require('./savedSearches');
const loggingResolvers = require('./logging');
const blogResolvers = require('./blog');
const seoPageResolvers = require('./seoPage');
const { GraphQLScalarType } = require('graphql');
const { Kind } = require('graphql/language');

const typeResolvers = {
    JSON: new GraphQLScalarType({
        name: 'JSON',
        description: 'JSON string that will be parsed',
        serialize(value) {
            return JSON.stringify(value);
        },
        parseValue(value) {
            return JSON.parse(value);
        },
        parseLiteral(ast) {
            if (ast.kind === Kind.OBJECT) {
                return ast.value;
            } else if (ast.kind === Kind.STRING) {
                return JSON.parse(ast.value);
            }
            return null;
        },
    }),
    Timestamp: new GraphQLScalarType({
        name: 'Timestamp',
        description: 'Timestamp as a string that will be parsed as a date',
        serialize(value) {
            return value.getTime();
        },
        parseValue(value) {
            return new Date(value);
        },
        parseLiteral(ast) {
            if (ast.kind === Kind.STRING) {
                return new Date(ast.value);
            }
            return null;
        },
    }),
};

module.exports = [
    feedbackResolvers,
    userResolvers,
    offersResolvers,
    savedSearchResolvers,
    loggingResolvers,
    blogResolvers,
    seoPageResolvers,
    typeResolvers,
];
