const {
    createToken,
    retrieveToken,
    currentTimestamp,
    RESPONSE_MESSAGES,
    hashPassword,
} = require('../../util');
const argon2 = require('argon2');

module.exports = {
    registerUser: async (
        parent,
        { email, emailConfirmation, password, type },
        { models }
    ) => {
        const createdAt = currentTimestamp();
        const newUser = await models.Users.createUser(email, password, {
            type,
            createdAt,
        });
        return { token: await createToken(newUser), user: newUser };
    },
    updateProfileInfo: async (
        parent,
        {
            token,
            skills,
            programmingLanguages,
            titles,
            locations,
            offerType,
            fileName,
        },
        { models }
    ) => {
        const { id } = await retrieveToken(token);
        await models.Users.update(
            {
                skills,
                programmingLanguages,
                titles,
                locations,
                offerType,
                fileName,
            },
            {
                where: {
                    id: id,
                },
            }
        );

        return await models.Users.findById(id);
    },
    updateAccountInfo: async (
        parent,
        { token, name, lastName, email, partnerParam, partnerValue },
        { models }
    ) => {
        const { id } = await retrieveToken(token);
        await models.Users.update(
            {
                name,
                lastName,
                email,
                partnerParam,
                partnerValue,
            },
            {
                where: {
                    id: id,
                },
            }
        );

        return await models.Users.findById(id);
    },
    updatePassword: async (
        parent,
        { token, password, newPassword },
        { models }
    ) => {
        const { id } = await retrieveToken(token);
        const user = await models.Users.findById(id);

        if (!(await argon2.verify(user.password, password))) {
            return { message: RESPONSE_MESSAGES.USER_BAD_PASSWORD };
        }

        await models.Users.update(
            {
                password: await hashPassword(newPassword),
            },
            {
                where: {
                    id: id,
                },
            }
        );

        return { message: RESPONSE_MESSAGES.USER_SUCCESS };
    },
    deleteUser: async (parent, { token }, { models, dataSources }) => {
        const { id } = await retrieveToken(token);
        const user = await models.Users.findById(id);
        if (user.file) {
            await dataSources.fileAPI.deleteFile(user.file);
        }

        await models.Users.destroy({
            where: {
                id: id,
            },
        });

        return { message: RESPONSE_MESSAGES.USER_REMOVED };
    },
};
