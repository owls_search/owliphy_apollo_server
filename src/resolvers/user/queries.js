const argon2 = require('argon2');
const { RESPONSE_MESSAGES, retrieveToken, createToken } = require('../../util');

module.exports = {
    user: async (parent, { token }, { models }) => {
        const { id } = await retrieveToken(token);
        return await models.Users.findById(id);
    },
    loginUser: async (parent, { email, password }, { models }) => {
        const user = await models.Users.findByEmail(email);
        if (!(await argon2.verify(user.password, password))) {
            return { message: RESPONSE_MESSAGES.USER_BAD_CREDENTIALS };
        }

        return { token: await createToken(user), user };
    },
};
