const queries = require('./queries.js');
const mutations = require('./mutations');

module.exports = {
    Query: queries,

    Mutation: mutations,

    User: {
        offers: async (user, args, { models }) => {
            return await models.Offers.findAll({
                where: {
                    createdUserId: user.id,
                },
            });
        },
        savedSearches: async (user, args, { models }) => {
            return await models.SavedSearches.findAll({
                where: {
                    userId: user.id,
                },
            });
        },
    },
};
