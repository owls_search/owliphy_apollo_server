const cors = require('cors');
const express = require('express');
const { parseAsync } = require('json2csv');
const getServer = require('./server');
const { sequelize, models } = require('./models');
const { init: initSentry, Handlers: SentryHandlers } = require('@sentry/node');
const morgan = require('morgan');
const MailingAPI = require('./api/mailing');
const FileAPI = require('./api/file');
const SearchAPI = require('./api/search');
const getSitemapOffers = require('./endpointServices/sitemapOffers');

if (process.env.ENABLE_SENTRY === 'true') {
    initSentry({
        dsn: 'https://e01030f4e5b1482aada1dd6c845df16a@sentry.io/1504947',
    });
}

const dataSources = {
    mailAPI: new MailingAPI(),
    fileAPI: new FileAPI(),
    searchAPI: new SearchAPI(),
};

module.exports = () => {
    const expressApp = express();
    expressApp.disable('x-powered-by');
    expressApp.use(cors());
    expressApp.use(morgan('short'));
    if (process.env.ENABLE_SENTRY === 'true') {
        expressApp.use(SentryHandlers.requestHandler());
        expressApp.use(SentryHandlers.errorHandler());
    }

    const server = getServer(models, dataSources);
    server.applyMiddleware({ app: expressApp, path: '/graphql' });

    sequelize.sync().then(async () => {
        expressApp.listen({ port: process.env.PORT || 4000 }, () => {
            console.log(`🚀 Server ready at ${server.graphqlPath}`);
        });

        expressApp.get('/sitemap_offers', getSitemapOffers);
    });
};
