const jwt = require('jsonwebtoken');
const argon2 = require('argon2');

const currentTimestamp = () => {
    const date = new Date();
    return date.setSeconds(date.getSeconds() + 1);
};

const createToken = async ({ id, type }) =>
    await jwt.sign({ id, type }, process.env.JWT_TOKEN_KEY || 'test_key_sign');
const retrieveToken = async token =>
    await jwt.verify(token, process.env.JWT_TOKEN_KEY || 'test_key_sign');

const hashPassword = async password =>
    await argon2.hash(password, {
        memoryCost: 1024,
        timeCost: 2,
        parallelism: 2,
    });

const RESPONSE_MESSAGES = {
    MISSING_TOKEN: 'message.missingToken',
    BAD_REQUEST: 'message.badRequest',

    JOB_NOT_FOUND: 'message.jobNotFound',
    JOB_FAILED: 'message.jobFail',
    JOB_SUCCESS_ADD: 'message.jobSuccessAdd',
    JOB_SUCCESS_UPDATE: 'message.jobSuccessUpdate',
    JOB_SUCCESS_REMOVE: 'message.jobSuccessRemove',

    USER_EXISTS: 'message.userExists',
    USER_FAILED: 'message.userFailed',
    USER_NOT_FOUND: 'message.userNotFound',
    USER_EMPTY_UPDATE_FIELDS: 'message.userEmptyUpdateFields',
    USER_BAD_CREDENTIALS: 'message.userBadCredentials',
    USER_BAD_PASSWORD: 'message.userBadPassword',
    USER_REMOVED: 'message.userRemoved',
    USER_SUCCESS: 'message.userSuccess',

    FEEDBACK_FAILED: 'message.feedbackFail',
    FEEDBACK_SUCCESS: 'message.feedbackSuccess',

    BLOG_POST_FAILED: 'message.postFailed',
    BLOG_POST_NOT_FOUND: 'message.postNotFound',

    SAVE_SEARCH_SUCCESS: 'message.saveSearchSuccess',
    SAVE_SEARCH_REMOVE_SUCCESS: 'message.saveSearchRemoveSuccess',
    SAVE_SEARCH_UPDATE_SUCCESS: 'message.saveSearchUpdateSuccess',
    SAVE_SEARCH_FAILED: 'message.saveSearchFailed',
};

const randomPassword = length =>
    Math.random()
        .toString(36)
        .slice(length * -1);

const isJestTest = () => {
    return process.env.JEST_WORKER_ID !== undefined;
};

const h1ToH2 = string =>
    string
        .split('<h1>')
        .join('<h2>')
        .split('</h1>')
        .join('</h2>');

module.exports = {
    retrieveToken,
    createToken,
    RESPONSE_MESSAGES,
    randomPassword,
    hashPassword,
    currentTimestamp,
    isJestTest,
    h1ToH2,
};
