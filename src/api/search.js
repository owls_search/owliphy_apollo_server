const { RESTDataSource } = require('apollo-datasource-rest');
const { head, omitBy, flow, isNil, property } = require('lodash');
const FormData = require('form-data');

const tagsToString = tags => tags.map(property('text')).join(', ');
const omitEmptyRequestProperties = obj => omitBy(obj, isNil);
const mapToSearchApiRequest = ({
    pageSize,
    page = 1,
    query,
    minBid = 0,
    language,
    createdUserId,
}) => ({
    page_size: pageSize, // eslint-disable-line camelcase
    page_index: page - 1, // eslint-disable-line camelcase
    bid: minBid, // eslint-disable-line camelcase
    created_user_id: createdUserId, // eslint-disable-line camelcase
    language,
    value: query,
});
const populateApiKey = req => ({
    ...req,
    'API-KEY': process.env.SEARCH_API_KEY,
});
const mapToFormData = req => {
    const fd = new FormData();
    Object.keys(req).map(key => fd.append(key, req[key]));
    return fd;
};

const mapToResponse = ({
    id,
    external_id,
    created_user_id,
    url,
    location,
    description,
    language,
    skills = [],
    programming,
    benefits,
    title,
    salary_from,
    salary_to,
    email = null,
    logo,
    name = '',
    created_at,
    updated_at,
    work_type,
    bid,
    import_partner_name,
    partner_tracking_param,
    partner_tracking_value,
    code,
}) => ({
    id,
    externalId: external_id,
    url,
    destinations: location,
    description,
    miniDescription: description,
    language,
    skills: tagsToString(skills),
    programmingLanguages: tagsToString(programming),
    benefits: tagsToString(benefits),
    title,
    startsFrom: null,
    salaryFrom: salary_from,
    salaryTo: salary_to,
    email,
    logo,
    companyName: name,
    createdAt: created_at,
    updatedAt: updated_at,
    offerType: work_type,
    htmlDescription: `<div>${description}</div>`,
    bid,
    isIndexed: true,
    createdUserId: created_user_id,
    importPartnerName: import_partner_name,
    partnerParam: partner_tracking_param,
    partnerValue: partner_tracking_value,
    code,
});

class SearchAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = `https://search.clusterjobs.de/v1/`;
    }

    async similarOffers({
        destinations,
        programmingLanguages,
        skills,
        benefits,
        title,
    }) {
        let query = `${destinations || ''} ${programmingLanguages ||
            skills ||
            ''} ${benefits || ''}`;

        if (!programmingLanguages && !skills && !benefits) {
            query = `${query} ${title}`;
        }

        const request = flow(
            mapToSearchApiRequest,
            populateApiKey,
            omitEmptyRequestProperties,
            mapToFormData
        )({
            query,
            pageSize: 15,
            language: 'all',
            page: 1,
        });

        try {
            const res = await this.post('listjobs/', request);
            const { jobs: offers = [] } = res;
            return offers.filter(offer => !isNil(offer)).map(mapToResponse);
        } catch (e) {
            return [];
        }
    }

    async search({
        query,
        minBid,
        createdUserId,
        pageSize = 15,
        language = 'all',
        page = 1,
    }) {
        const request = flow(
            head,
            mapToSearchApiRequest,
            populateApiKey,
            omitEmptyRequestProperties,
            mapToFormData
        )(arguments);

        try {
            const res = await this.post('listjobs/', request);
            const { jobs: offers = [] } = res;
            return offers.map(mapToResponse);
        } catch (e) {
            console.log(e);
            return [];
        }
    }

    async recentOffers() {
        const request = populateApiKey({});

        try {
            const res = await this.get('mainpage/', request);
            const { jobs: offers = [] } = res;
            return offers.map(mapToResponse);
        } catch (e) {
            return [];
        }
    }
}

module.exports = SearchAPI;
