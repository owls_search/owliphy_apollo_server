const { RESTDataSource } = require('apollo-datasource-rest');

const types = {
    SAVE_SEARCH_NEWCOMER: 'newcomerSavedSearch',
    SAVE_SEARCH: 'newSavedSearch',
    FEEDBACK: 'feedback',
};

class MailingAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = `${process.env.PHP_API_ENDPOINT}/api/mail/`;
    }

    willSendRequest(request) {
        request.body.key = process.env.PHP_API_KEY;
    }

    async sendMail(type, to, subject, variables) {
        return this.post('send', {
            to,
            subject,
            variables,
            type,
        });
    }

    async sendToSaveSearchNewcomer(email, query, password) {
        return this.sendMail(
            types.SAVE_SEARCH_NEWCOMER,
            email,
            'clusterjobs - we made a cabinet for you!',
            {
                query,
                email,
                password,
            }
        );
    }

    async sendAfterNewSavedSearch(email, query) {
        return this.sendMail(
            types.SAVE_SEARCH,
            email,
            'clusterjobs - your search has been saved',
            {
                query,
            }
        );
    }

    async sendAfterFeedback(email, message) {
        return this.sendMail(
            types.FEEDBACK,
            email,
            'clusterjobs - your feedback has been sent!',
            {
                email,
                message,
            }
        );
    }
}

module.exports = MailingAPI;
