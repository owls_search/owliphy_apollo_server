const { RESTDataSource } = require('apollo-datasource-rest');

class FileAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = `${process.env.PHP_API_ENDPOINT}/api/file/`;
    }

    willSendRequest(request) {
        request.body.key = process.env.PHP_API_KEY;
    }

    async deleteFile(fileName) {
        return this.post('delete', { fileName });
    }
}

module.exports = FileAPI;
