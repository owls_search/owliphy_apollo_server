module.exports = function(sequelize, DataTypes) {
    return sequelize.define(
        'logs_to_details',
        {
            id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            logId: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                references: {
                    model: 'logs',
                    key: 'id',
                },
                field: 'log_id',
            },
            detailId: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                references: {
                    model: 'logs_details',
                    key: 'id',
                },
                field: 'detail_id',
            },
            value: {
                type: DataTypes.STRING(191),
                allowNull: true,
            },
        },
        {
            timestamps: false,
            tableName: 'logs_to_details',
        }
    );
};
