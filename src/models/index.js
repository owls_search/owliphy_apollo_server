const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(
    process.env.DB_DATABASE,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    {
        dialect: 'mysql',
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        logging: null,
    }
);

const models = {
    Users: sequelize.import('./users'),
    Offers: sequelize.import('./offers'),
    SavedSearches: sequelize.import('./saved_searches.js'),
    Posts: sequelize.import('./posts'),
    PostsTranslations: sequelize.import('./posts_translates.js'),
    Feedback: sequelize.import('./feedback.js'),
    LogsDetails: sequelize.import('./logs_details.js'),
    LogsEvents: sequelize.import('./logs_events.js'),
    Logs: sequelize.import('./logs.js'),
    LogsToDetails: sequelize.import('./logs_to_details.js'),
    JobsAnalysisArticles: sequelize.import('./jobs_analysis_articles.js'),
};

Object.keys(models).forEach(key => {
    if ('associate' in models[key]) {
        models[key].associate(models);
    }
});

module.exports = { sequelize, models };
