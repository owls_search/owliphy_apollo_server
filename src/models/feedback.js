module.exports = function(sequelize, DataTypes) {
    return sequelize.define(
        'feedback',
        {
            id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING(191),
                allowNull: true,
            },
            email: {
                type: DataTypes.STRING(191),
                allowNull: true,
            },
            message: {
                type: DataTypes.STRING(191),
                allowNull: false,
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'created_at',
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'updated_at',
            },
        },
        {
            tableName: 'feedback',
        }
    );
};
