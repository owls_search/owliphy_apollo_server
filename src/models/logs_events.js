module.exports = function(sequelize, DataTypes) {
    return sequelize.define(
        'logs_events',
        {
            id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                autoIncrement: true,
                allowNull: false,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING(191),
                allowNull: false,
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'created_at',
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'updated_at',
            },
        },
        {
            tableName: 'logs_events',
        }
    );
};
