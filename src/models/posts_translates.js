module.exports = function(sequelize, DataTypes) {
    const PostTranslations = sequelize.define(
        'posts_translates',
        {
            id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            title: {
                type: DataTypes.STRING(191),
                allowNull: false,
            },
            text: {
                type: DataTypes.TEXT,
                allowNull: false,
            },
            language: {
                type: DataTypes.STRING(2),
                allowNull: false,
            },
            postId: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                references: {
                    model: 'posts',
                    key: 'id',
                },
                field: 'post_id',
            },
            preview: {
                type: DataTypes.TEXT,
                allowNull: false,
            },
            code: {
                type: DataTypes.TEXT,
                allowNull: false,
            },
            seoTitle: {
                type: DataTypes.STRING(100),
                allowNull: false,
                field: 'seo_title',
            },
            seoKeywords: {
                type: DataTypes.STRING(400),
                allowNull: false,
                field: 'seo_keywords',
            },
            seoDescription: {
                type: DataTypes.STRING(400),
                allowNull: false,
                field: 'seo_description',
            },
            postTags: {
                type: DataTypes.STRING(400),
                allowNull: false,
                field: 'post_tags',
            },
        },
        {
            timestamps: false,
            tableName: 'posts_translates',
        }
    );

    PostTranslations.associate = models => {
        models.PostsTranslations.belongsTo(models.Posts, {
            onDelete: 'CASCADE',
            foreignKey: {
                fieldName: 'post_id',
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: true,
                require: true,
            },
            targetKey: 'id',
        });
    };

    return PostTranslations;
};
