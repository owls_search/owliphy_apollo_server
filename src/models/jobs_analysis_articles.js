'use strict';
module.exports = (sequelize, DataTypes) => {
    const JobsAnalysisArticles = sequelize.define(
        'jobs_analysis_articles',
        {
            id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            language: {
                type: DataTypes.STRING(2),
                allowNull: false,
            },
            code: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            programming: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            location: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            content: {
                type: DataTypes.TEXT,
                allowNull: false,
                field: 'html_article',
            },
        },
        { timestamps: false }
    );
    JobsAnalysisArticles.associate = function(models) {
        // associations can be defined here
    };
    return JobsAnalysisArticles;
};
