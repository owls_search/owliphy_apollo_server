module.exports = function(model) {
    model.findById = async id => {
        return await model.findOne({
            where: { id },
        });
    };

    return model;
};
