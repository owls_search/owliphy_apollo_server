const extendModelAPI = require('./extend.js');

module.exports = function(sequelize, DataTypes) {
    const Offers = sequelize.define(
        'jobs_unique',
        {
            id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            externalId: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'external_id',
            },
            url: {
                type: DataTypes.STRING(4096),
                allowNull: true,
            },
            destinations: {
                type: DataTypes.TEXT,
                allowNull: true,
                field: 'location',
            },
            description: {
                type: DataTypes.TEXT,
                allowNull: true,
            },
            miniDescription: {
                type: DataTypes.TEXT,
                allowNull: true,
                field: 'mini_description',
            },
            language: {
                type: DataTypes.STRING(191),
                allowNull: true,
            },
            skills: {
                type: DataTypes.TEXT,
                allowNull: true,
            },
            programmingLanguages: {
                type: DataTypes.TEXT,
                allowNull: true,
                field: 'programming',
            },
            title: {
                type: DataTypes.STRING(191),
                allowNull: true,
            },
            originalTitle: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'original_title',
            },
            startsFrom: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'starts_from',
            },
            salaryFrom: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: 'salary_from',
            },
            salaryTo: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: 'salary_to',
            },
            email: {
                type: DataTypes.STRING(191),
                allowNull: true,
            },
            logo: {
                type: DataTypes.STRING(4096),
                allowNull: true,
            },
            companyName: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'company_name',
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'created_at',
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'created_at',
            },
            offerType: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'work_type',
            },
            code: {
                type: DataTypes.STRING(255),
                allowNull: true,
                field: 'code',
            },
            createdUserId: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'created_user_id',
            },
            htmlDescription: {
                type: DataTypes.TEXT,
                allowNull: true,
                field: 'html_description',
            },
            isIndexed: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: '0',
                field: 'is_indexed',
            },
            benefits: {
                type: DataTypes.STRING(191),
                allowNull: true,
            },
            bid: {
                type: 'DOUBLE(8,2)',
                allowNull: true,
                field: 'dynamic_bid',
            },
        },
        {
            tableName: 'jobs',
        }
    );

    return extendModelAPI(Offers);
};
