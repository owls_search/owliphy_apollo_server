module.exports = function(sequelize, DataTypes) {
    return sequelize.define(
        'logs',
        {
            id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            timestamp: {
                type: DataTypes.DATE,
                allowNull: false,
            },
            eventId: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                references: {
                    model: 'logs_events',
                    key: 'id',
                },
                field: 'event_id',
            },
        },
        {
            timestamps: false,
            tableName: 'logs',
        }
    );
};
