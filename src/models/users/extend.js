const { UserInputError } = require('apollo-server');
const { isEmpty } = require('lodash');
const { hashPassword, RESPONSE_MESSAGES } = require('../../util/index.js');

module.exports = function(model) {
    model.findById = async id => {
        return await model.findOne({
            where: { id },
        });
    };

    model.findByEmail = async email => {
        return await model.findOne({
            where: { email },
        });
    };

    model.createUser = async (email, password, props = {}) => {
        if (!isEmpty(await model.findByEmail(email))) {
            throw new UserInputError(RESPONSE_MESSAGES.USER_EXISTS);
        }

        return await model.create({
            email,
            password: await hashPassword(password),
            ...props,
        });
    };

    return model;
};
