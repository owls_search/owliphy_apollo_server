const extendModelAPI = require('./extend.js');

module.exports = function(sequelize, DataTypes) {
    const User = sequelize.define(
        'users',
        {
            id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING(191),
                allowNull: true,
            },
            lastName: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'last_name',
            },
            email: {
                type: DataTypes.STRING(191),
                allowNull: false,
                unique: true,
                validate: {
                    notEmpty: true,
                    isEmail: true,
                },
            },
            password: {
                type: DataTypes.STRING(191),
                allowNull: false,
                validate: {
                    notEmpty: true,
                },
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'created_at',
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'updated_at',
            },
            skills: {
                type: DataTypes.TEXT,
                allowNull: true,
            },
            titles: {
                type: DataTypes.TEXT,
                allowNull: true,
                field: 'job_titles',
            },
            offerType: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'type_of_work',
            },
            fileName: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'file',
            },
            type: {
                type: DataTypes.STRING(191),
                allowNull: true,
            },
            locations: {
                type: DataTypes.TEXT,
                allowNull: true,
                field: 'job_locations',
            },
            programmingLanguages: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'programming_languages',
            },
            partnerParam: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'partner_tracking_param',
            },
            partnerValue: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'partner_tracking_value',
            },
            partnerBid: {
                type: 'DOUBLE(8,2)',
                allowNull: true,
                field: 'partner_bid',
            },
            partnerCompanyName: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'partner_company_name',
            },
            partnerCompanyLogo: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'partner_company_logo',
            },
            importPartnerName: {
                type: DataTypes.STRING(191),
                allowNull: true,
                field: 'import_partner_name',
            },
        },
        {
            timestamps: true,
            underscored: true,
            tableName: 'users',
        }
    );

    return extendModelAPI(User);
};
