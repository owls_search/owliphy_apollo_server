module.exports = function(sequelize, DataTypes) {
    return sequelize.define(
        'saved_searches',
        {
            id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'created_at',
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'updated_at',
            },
            userId: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'id',
                },
                field: 'user_id',
            },
            query: {
                type: DataTypes.TEXT,
                allowNull: false,
            },
            sendDateOfMonth: {
                type: DataTypes.STRING(100),
                allowNull: true,
                field: 'send_date_of_month',
            },
            sendWeeklyMonday: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: '0',
                field: 'send_weekly_monday',
            },
            sendWeeklyTuesday: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: '0',
                field: 'send_weekly_tuesday',
            },
            sendWeeklyWednesday: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: '0',
                field: 'send_weekly_wednesday',
            },
            sendWeeklyThursday: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: '0',
                field: 'send_weekly_thursday',
            },
            sendWeeklyFriday: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: '0',
                field: 'send_weekly_friday',
            },
            sendWeeklySaturday: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: '0',
                field: 'send_weekly_saturday',
            },
            sendWeeklySunday: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: '0',
                field: 'send_weekly_sunday',
            },
        },
        {
            tableName: 'saved_searches',
        }
    );
};
