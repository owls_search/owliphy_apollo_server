const extendModelAPI = require('./extend.js');

module.exports = function(sequelize, DataTypes) {
    const Posts = sequelize.define(
        'posts',
        {
            id: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            imageBig: {
                type: DataTypes.STRING(191),
                allowNull: false,
                field: 'image_big',
            },
            imageNormal: {
                type: DataTypes.STRING(191),
                allowNull: false,
                field: 'image_normal',
            },
            imageSmall: {
                type: DataTypes.STRING(191),
                allowNull: false,
                field: 'image_small',
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'created_at',
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'updated_at',
            },
            active: {
                type: DataTypes.INTEGER(1),
                allowNull: false,
                defaultValue: '1',
            },
        },
        {
            tableName: 'posts',
        }
    );

    Posts.associate = models => {
        models.Posts.hasMany(models.PostsTranslations, {
            onDelete: 'CASCADE',
            foreignKey: {
                fieldName: 'id',
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                require: true,
            },
            as: 'posts_translations',
            targetKey: 'post_id',
        });
    };

    return extendModelAPI(Posts);
};
