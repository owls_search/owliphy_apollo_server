const { ApolloServer } = require('apollo-server-express');
const schema = require('./schema/index.js');
const { isJestTest } = require('./util/index.js');
const resolvers = require('./resolvers/index.js');
const responseCachePlugin = require('apollo-server-plugin-response-cache');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

module.exports = (models, dataSources) => {
    const serverOptions = {
        typeDefs: schema,
        resolvers,
        engine: false,
        plugins: [],
        dataSources: () => dataSources,
        context: () => ({
            models,
        }),
    };

    if (!isJestTest()) {
        serverOptions.plugins.push(responseCachePlugin());
        if (process.env.ENGINE_API_KEY) {
            serverOptions.engine = {
                apiKey: process.env.APOLLO_ENGINE_API,
                schemaTag: process.env.NODE_ENV === 'prod' ? 'live' : 'beta',
            };
        }
    }

    return new ApolloServer(serverOptions);
};
