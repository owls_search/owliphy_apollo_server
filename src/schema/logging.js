const { gql } = require('apollo-server-express');

module.exports = gql`
    extend type Mutation {
        log(event: Int!, details: JSON!, timestamp: Timestamp!): Boolean
    }
`;
