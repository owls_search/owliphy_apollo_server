const { gql } = require('apollo-server-express');

module.exports = gql`
    extend type Mutation {
        addFeedback(email: String!, message: String!, name: String!): Message!
    }
`;
