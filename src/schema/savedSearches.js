const { gql } = require('apollo-server-express');

module.exports = gql`
    extend type Query {
        saveSearchAdd(
            token: String!
            query: String!
            isWeekly: Boolean!
            dayOfMonth: String
            dayOfWeek: String
        ): SavedSearchResult!
        saveSearchUpdate(
            token: String!
            id: ID!
            query: String
            isWeekly: Boolean
            dayOfMonth: String
            dayOfWeek: String
        ): SavedSearchResult
        saveSearchRemove(
            token: String!
            id: ID!
        ): Message!
        saveSearchAddNewcomer(
            email: String!
            query: String!
            isWeekly: Boolean!
            dayOfMonth: String
            dayOfWeek: String
        ): AuthenticationResult!
    }

    type SavedSearchResult {
        result: Message!
        savedSearch: SavedSearch!
    }
    
    type SavedSearch {
        id: ID!
        userId: Int!
        query: String!
        schedule: SavedSearchSchedule!
    }

    type SavedSearchSchedule {
        isWeekly: Boolean!
        weekDays: [String]
        monthDays: [String]
    }
`;
