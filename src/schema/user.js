const { gql } = require('apollo-server-express');

module.exports = gql`
    extend type Query {
        user(token: String!): User
        loginUser(email: String!, password: String!): AuthenticationResult
    }

    extend type Mutation {
        registerUser(
            email: String!
            emailConfirmation: String!
            password: String!
            type: String!
        ): AuthenticationResult!
        updatePassword(
            token: String!
            password: String!
            newPassword: String!
        ): Message!
        deleteUser(token: String!): Message!
        updateAccountInfo(
            token: String!
            name: String
            lastName: String
            email: String
            partnerParam: String
            partnerValue: String
        ): User!
        updateProfileInfo(
            token: String!
            skills: String
            programmingLanguages: String
            titles: String
            locations: String
            offerType: String
            fileName: String
        ): User!
    }

    type AuthenticationResult {
        token: String
        user: User
    }

    input UserInput {
        name: String
        lastName: String
        email: String
        password: String
        skills: String
        programmingLanguages: String
        titles: String
        locations: String
        offerType: String
        type: String
        fileName: String
        partnerParam: String
        partnerValue: String
        partnerCompanyName: String
        partnerCompanyLogo: String
    }

    type User {
        id: ID!
        name: String
        lastName: String
        email: String!
        skills: String
        programmingLanguages: String
        titles: String
        locations: String
        offerType: String
        type: String
        fileName: String
        partnerParam: String
        partnerValue: String
        partnerCompanyName: String
        partnerCompanyLogo: String
        offers: [Offer!]
        savedSearches: [SavedSearch!]
    }
`;
