const { gql } = require('apollo-server-express');

module.exports = gql`
    extend type Query {
        getSeoPage(code: String!): JobAnalysis!
        findPopularJobs(city: String!, lang: String!): [JobAnalysis]
    }
`;
