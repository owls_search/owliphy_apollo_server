const { gql } = require('apollo-server-express');

module.exports = gql`
    extend type Query {
        getPost(code: String!): PostObject
        getRecentPosts(locale: String!): [PostObject!]
    }

    type PostObject {
        title: String!
        preview: String!
        language: String!
        code: String!
        postTags: String
        text: String!
        seoTitle: String!
        seoKeywords: String!
        seoDescription: String!
        images: PostImages!
        translatesCodes: JSON!
        nextPostCode: String
    }

    type PostImages {
        small: String!
        normal: String!
        big: String!
    }
`;
