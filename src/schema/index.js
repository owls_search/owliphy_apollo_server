const { gql } = require('apollo-server-express');

const feedbackSchema = require('./feedback');
const loggingSchema = require('./logging');
const blogSchema = require('./blog');
const userSchema = require('./user');
const offerSchema = require('./offers');
const savedSearchesSchema = require('./savedSearches');
const seoPageSchema = require('./seoPage');

const linkSchema = gql`
    scalar JSON
    scalar Timestamp

    type Query {
        _: Boolean
    }

    type Mutation {
        _: Boolean
    }

    type Subscription {
        _: Boolean
    }

    type Message {
        message: String!
    }

    type OfferId {
        id: Int!
    }

    type OffersList {
        totalNumber: Int!
        offers: [Offer!]
    }

    type JobAnalysis {
        id: ID!
        code: String!
        content: String!
        language: String!
        location: String!
        programming: String
    }
`;

module.exports = [
    linkSchema,
    userSchema,
    offerSchema,
    savedSearchesSchema,
    blogSchema,
    loggingSchema,
    feedbackSchema,
    seoPageSchema,
];
