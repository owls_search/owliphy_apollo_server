const { gql } = require('apollo-server-express');

module.exports = gql`
    extend type Query {
        offer(id: ID!): Offer
        getHomepageConfiguration(locale: String!): HomepageConfiguration!
        getUserOffers(page: Int!, token: String!): OffersList!
    }

    extend type Mutation {
        addOffer(token: String!, offer: OfferInput!): OfferId!
        deleteOffer(id: ID!, token: String!): Message!
        updateOffer(token: String!, id: ID!, offer: OfferInput!): Message!
    }

    type HomepageConfiguration {
        recentOffers: [Offer!]
        seoPages: [JobAnalysis!]
    }

    type Offer {
        id: ID!
        externalId: String
        url: String!
        email: String
        destinations: String
        programmingLanguages: String
        skills: String
        benefits: String
        title: String!
        originalTitle: String
        htmlDescription: String
        description: String!
        miniDescription: String
        startsFrom: String
        salaryFrom: String
        salaryTo: String
        companyName: String!
        offerType: String
        logo: String
        language: String
        isIndexed: Boolean!
        bid: Float
        importPartnerName: String
        createdUserId: Int!
        partnerParam: String
        partnerValue: String
        code: String
        createdAt: String!
        similarOffers: [Offer]
        popularJobs: [JobAnalysis]
    }

    input OfferInput {
        title: String!
        description: String!
        miniDescription: String
        skills: String
        programmingLanguages: String
        destinations: String
        url: String!
        companyName: String!
        offerType: String
        startsFrom: String
        email: String
        salaryFrom: Int
        salaryTo: Int
        logo: String
        language: String
    }
`;
